SET autocommit = 0;

CREATE SCHEMA IF NOT EXISTS `LittleLemonDB` ;
USE `LittleLemonDB` ;

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `contact_number` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `LittleLemonDB`.`customer` VALUES 
  (1, 'Juan', 'Rojas', '5550987', 'juan@rojas.com'),
  (2, 'Maria', 'Verdes', '5551234', 'maria@verdes.com'),
  (3, 'Camilo', 'Blancos', '5554321', 'camilo@blancos.com'),
  (4, 'Carmen', 'Amarillos', '5559876', 'carmen@amarillos.com');

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`bench` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `seatings` INT NOT NULL DEFAULT 4,
  `requires_reservation` TINYINT NOT NULL DEFAULT 0,
  `is_outside` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

INSERT INTO `LittleLemonDB`.`bench` VALUES 
  (1, 2, 1, 0),
  (2, 2, 1, 0),
  (3, 2, 1, 0),
  (4, 4, 1, 0),
  (5, 4, 0, 0),
  (6, 4, 0, 0),
  (7, 6, 0, 0),
  (8, 6, 0, 1),
  (9, 8, 0, 1),
  (10, 8, 0, 0),
  (11, 12, 1, 1),
  (12, 12, 1, 1);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `slot` TIME NOT NULL,
  `bench_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_2_idx` (`customer_id` ASC) VISIBLE,
  INDEX `fk_booking_3_idx` (`bench_id` ASC) VISIBLE,
  CONSTRAINT `fk_booking_2`
    FOREIGN KEY (`customer_id`)
    REFERENCES `LittleLemonDB`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_3`
    FOREIGN KEY (`bench_id`)
    REFERENCES `LittleLemonDB`.`bench` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`booking` VALUES 
  (1, '2023-3-25', '20:00:00', 1, 1),
  (2, '2023-3-25', '19:45:00', 2, 2),
  (3, '2023-3-25', '21:00:00', 5, 3),
  (4, '2023-3-25', '20:30:00', 7, 4);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `LittleLemonDB`.`city` VALUES 
  (1, 'Medellin');

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `anual_salary` DECIMAL(8,2) NOT NULL,
  `overtime` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `LittleLemonDB`.`role` VALUES 
  (1, 'Manager', 75000, 35.5),
  (2, 'chef', 57000, 40.5),
  (3, 'Waitperson', 45000, 17.5),
  (4, 'Roundsman', 42000, 22.5);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`staff` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `role_id` INT NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_staff_1_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_staff_2_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_staff_1`
    FOREIGN KEY (`city_id`)
    REFERENCES `LittleLemonDB`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_staff_2`
    FOREIGN KEY (`role_id`)
    REFERENCES `LittleLemonDB`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`staff` VALUES 
  (1, 'Lorena','Lemile', 1, '123 fake street', 1),
  (2, 'Carlos','Casas', 2, '234 fake street', 1),
  (3, 'Laura','Smith', 2, '234 fake street', 1),
  (4, 'Lina','Lawrence', 3, '232 fake street', 1),
  (5, 'Santiago','Silva', 3, '231 fake street', 1),
  (6, 'Steven','Smith', 3, '111 fake street', 1),
  (7, 'Carl','Johnson', 4, '222 fake street', 1),
  (8, 'Cindy','Campbel', 4, '232 fake street', 1);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` DATE NOT NULL,
  `restaurant_state` ENUM('Received', 'Canceled', 'Attended', 'Paid') NULL,
  `delivery_state` ENUM('Received', 'Canceled', 'Preparing', 'On delivery', 'Delivered') NULL,
  `staff_id` INT NOT NULL,
  `customer_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_1_idx` (`staff_id` ASC) VISIBLE,
  INDEX `fk_order_2_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `LittleLemonDB`.`staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_2`
    FOREIGN KEY (`customer_id`)
    REFERENCES `LittleLemonDB`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`order` VALUES 
  (1, '2023-03-25 12:00:00','2023-03-24', 'Paid', NULL, 4, 1),
  (2, '2023-03-25 12:00:00','2023-03-24', 'Paid', NULL, 5, 4),
  (3, '2023-03-25 13:00:00','2023-03-24', 'Paid', NULL, 4, 3),
  (4, '2023-03-25 13:00:00','2023-03-24', 'Paid', NULL, 6, 2),
  (5, '2023-03-25 14:00:00','2023-03-24', 'Paid', NULL, 6, 1),
  (6, '2023-03-25 11:00:00','2023-03-24', NULL, 'Delivered', 8, 1),
  (7, '2023-03-25 13:00:00','2023-03-24', NULL, 'Delivered', 8, 2),
  (8, '2023-03-25 13:30:00','2023-03-24', NULL, 'Delivered', 7, 3),
  (9, '2023-03-25 14:00:00','2023-03-24', NULL, 'Delivered', 7, 4);


CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`delivery` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stimated_delivery_time` DATETIME NOT NULL,
  `order_id` INT NOT NULL,
  `delivered_time` DATETIME NULL,
  `address` VARCHAR(255) NOT NULL,
  `city_id` INT NOT NULL,
  `delivery_cost` DECIMAL(5,2) NOT NULL DEFAULT 0.0,
  `staff_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_delivery_1_idx` (`order_id` ASC) VISIBLE,
  INDEX `fk_delivery_2_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_delivery_3_idx` (`staff_id` ASC) VISIBLE,
  CONSTRAINT `fk_delivery_1`
    FOREIGN KEY (`order_id`)
    REFERENCES `LittleLemonDB`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_2`
    FOREIGN KEY (`city_id`)
    REFERENCES `LittleLemonDB`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_3`
    FOREIGN KEY (`staff_id`)
    REFERENCES `LittleLemonDB`.`staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`delivery` VALUES 
  (1, '2023-03-25 11:00:00', '2023-03-25 12:00:00',6, '2023-03-25 12:05:00', '117 fake street', 1, 0.0, 8),
  (2, '2023-03-25 13:00:00', '2023-03-25 14:00:00',7, '2023-03-25 14:00:00', '113 fake street', 1, 0.0, 8),
  (3, '2023-03-25 13:30:00', '2023-03-25 14:20:00',8, '2023-03-25 14:19:00', '116 fake street', 1, 0.0, 7),
  (4, '2023-03-25 14:00:00', '2023-03-25 14:55:00',9, '2023-03-25 14:55:00', '115 fake street', 1, 0.0, 7);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `LittleLemonDB`.`category` VALUES 
  (1, 'Dessert'),
  (2, 'Salad'),
  (3, 'Meat'),
  (4, 'Drinks');

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`subcategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `category_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_subcategory_1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_subcategory_1`
    FOREIGN KEY (`category_id`)
    REFERENCES `LittleLemonDB`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`subcategory` VALUES 
  (1, 'Cake', 1),
  (2, 'Milk based', 1),
  (3, 'Cold', 1),
  (4, 'Healty', 2),
  (5, 'Protein suplement', 2),
  (6, 'White meat', 3),
  (7, 'Red meat', 3),
  (8, 'Mixed', 3),
  (9, 'Alcoholic', 4),
  (10, 'Non-Alcoholic', 3);

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL DEFAULT 0.0,
  `description` TEXT NULL,
  `subcategory_id` INT NOT NULL,
  `is_vegan` TINYINT NOT NULL DEFAULT 0,
  `cuisine` VARCHAR(100) NOT NULL,
  `dish_type` ENUM('Starter', 'Course', 'Drink', 'Dessert') NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_menu_1_idx` (`subcategory_id` ASC) VISIBLE,
  CONSTRAINT `fk_menu_1`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `LittleLemonDB`.`subcategory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`menu` VALUES 
  (1, 'Baked chicken with spinach and tomatos well cooked', 8.35, NULL, 6, 0, 'American', 'Course'),
  (2, 'A vegetarian dish to enjoy the flavor of an excellent pumpkin', 4.5, NULL, 4, 1, 'American', 'Course'),
  (3, 'Baked salmon with a lemon sauce with a perfect pinch of salt', 10.75, NULL, 6, 0, 'American', 'Course'),
  (4, 'A sweet cheesecake with berries', 5.45, NULL, 2, 0, 'American', 'Dessert'),
  (5, 'The exclusive dessert of lime chocolate', 6.1, NULL, 3, 1, 'American', 'Dessert'),
  (6, 'An excellent deersteak with potatoes', 11.85, NULL, 7, 0, 'American', 'Course'),
  (7, 'A vanilla dessert with dots of strawberries', 4.5, NULL, 3, 1, 'American', 'Dessert'),
  (8, 'a fired breef with a house´s selection spices', 6.3, NULL, 7, 0, 'American', 'Course'),
  (9, 'A greek salad with a lot of vegetables', 6.55, NULL, 4, 1, 'American', 'Course'),
  (10, 'A vanilla ice cream scoop with brownies', 5.1, NULL, 2, 0, 'American', 'Starter'),
  (11, 'The main desset of the house, "The Little Lemon Dessert"', 6.35, NULL, 3, 1, 'American', 'Dessert'),
  (12, 'Mushroom cream with a lot of vegetables and chicken', 7.45, NULL, 6, 0, 'American', 'Course'),
  (13, 'Panna cotta with berries', 9.15, NULL, 5, 1, 'American', 'Starter'),
  (14, 'Potatoes with chicken with tomato sauce', 7.95, NULL, 6, 0, 'American', 'Course'),
  (15, 'Pumpkin mincemeat with tomato', 8.55, NULL, 4, 1, 'American', 'Starter'),
  (16, 'Roasted meat with tomato sauce and chicken and pork', 10.5, NULL, 8, 0, 'American', 'Course'),
  (17, 'Salmon with a lot vegetables', 12.15, NULL, 6, 0, 'American', 'Course'),
  (18, 'Bowl of traditional fried potatoes with meat', 7, NULL, 7, 0, 'American', 'Course'),
  (19, 'An excellent steak with mashed potatoes', 13.5, NULL, 7, 0, 'American', 'Course'),
  (20, '1987 Wine', 33.5, NULL, 9, 0, 'American', 'Drink'),
  (21, 'Ginebra', 23.5, NULL, 9, 0, 'American', 'Drink'),
  (22, 'Strawberry soda', 3.5, NULL, 10, 0, 'American', 'Drink'),
  (23, 'Mango juice', 3.5, NULL, 10, 0, 'American', 'Drink');

CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`order_items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `menu_id` INT NOT NULL,
  `quantity` INT NOT NULL DEFAULT 1,
  `price` DECIMAL(5,2) NOT NULL,
  `canceled` TINYINT NOT NULL DEFAULT 0,
  `discount` DECIMAL(3,2) NOT NULL DEFAULT 0.0,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_items_1_idx` (`menu_id` ASC) VISIBLE,
  INDEX `fk_order_items_2_idx` (`order_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_items_1`
    FOREIGN KEY (`menu_id`)
    REFERENCES `LittleLemonDB`.`menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_items_2`
    FOREIGN KEY (`order_id`)
    REFERENCES `LittleLemonDB`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `LittleLemonDB`.`order_items` VALUES 
  (1,1, 3, 8.35, 0, 0.0, 1),
  (2,3, 2, 10.75, 0, 0.0, 1),
  (3,4, 1, 5.45, 0, 0.0, 1),
  (4,5, 1, 6.1, 0, 0.0, 1),
  (5,6, 3, 11.85, 0, 0.0, 1),
  (6,11, 3, 6.35, 0, 0.0, 1),
  (7,13, 2, 9.15, 0, 0.0, 1),
  (8,14, 2, 7.95, 0, 0.0, 1),
  (9,15, 2, 8.55, 0, 0.0, 1),
  (10,16, 3, 10.5, 0, 0.0, 1),
  (11,20, 2, 33.5, 0, 0.0, 1),
  (12,23, 4, 3.5, 0, 0.0, 1),
  (13,2, 1, 4.5, 0, 0.0, 2),
  (14,7, 2, 4.5, 0, 0.0, 2),
  (15,8, 1, 6.3, 0, 0.0, 2),
  (16,9, 3, 6.55, 0, 0.0, 2),
  (17,11, 1, 6.35, 0, 0.0, 2),
  (18,10, 1, 5.1, 0, 0.0, 2),
  (19,22, 2, 3.5, 0, 0.0, 2),
  (20,21, 2, 23.5, 0, 0.0, 2),
  (21,1, 1, 8.35, 0, 0.0, 3),
  (22,3, 1, 4.5, 0, 0.0, 3),
  (23,6, 1, 11.85, 0, 0.0, 3),
  (24,7, 2, 4.5, 0, 0.0, 3),
  (25,12, 1, 7.45, 0, 0.0, 3),
  (26,14, 3, 7.95, 0, 0.0, 3),
  (27,20, 1, 33.5, 0, 0.0, 3),
  (28,13, 1, 9.15, 0, 0.0, 4),
  (29,15, 1, 8.55, 0, 0.0, 4),
  (30,4, 1, 5.45, 0, 0.0, 4),
  (31,5, 2, 6.1, 0, 0.0, 4),
  (32,6, 1, 11.85, 0, 0.0, 4),
  (33,9, 2, 6.55, 0, 0.0, 4),
  (34,2, 2, 4.5, 0, 0.0, 4),
  (35,23, 5, 3.5, 0, 0.0, 4),
  (36,16, 2, 10.5, 0, 0.0, 5),
  (37,14, 1, 7.95, 0, 0.0, 5),
  (38,13, 1, 9.15, 0, 0.0, 5),
  (39,17, 2, 12.15, 0, 0.0, 5),
  (40,7, 1, 4.5, 0, 0.0, 5),
  (41,8, 1, 6.3, 0, 0.0, 5),
  (42,23, 3, 3.5, 0, 0.0, 5),
  (43,11, 1, 6.35, 0, 0.0, 6),
  (44,10, 1, 5.1, 0, 0.0, 6),
  (45,17, 2, 12.15, 0, 0.0, 6),
  (46,19, 3, 13.5, 0, 0.0, 6),
  (47,9, 1, 6.55, 0, 0.0, 6),
  (48,18, 1, 7, 0, 0.0, 6),
  (49,22, 5, 3.5, 0, 0.0, 6),
  (50,4, 3, 5.45, 0, 0.0, 7),
  (51,6, 1, 11.85, 0, 0.0, 7),
  (52,8, 1, 6.3, 0, 0.0, 7),
  (53,9, 2, 6.55, 0, 0.0, 7),
  (54,13, 1, 9.15, 0, 0.0, 7),
  (55,15, 1, 8.55, 0, 0.0, 7),
  (56,19, 1, 13.5, 0, 0.0, 7),
  (57,21, 5, 23.5, 0, 0.0, 7),
  (58,18, 1, 7, 0, 0.0, 8),
  (59,3, 1, 10.75, 0, 0.0, 8),
  (60,6, 1, 11.85, 0, 0.0, 8),
  (61,7, 2, 4.5, 0, 0.0, 8),
  (62,1, 2, 8.35, 0, 0.0, 8),
  (63,11, 1, 6.35, 0, 0.0, 8),
  (64,10, 3, 5.1, 0, 0.0, 8),
  (65,22, 5, 3.5, 0, 0.0, 8),
  (66,7, 3, 4.5, 0, 0.0, 9),
  (67,4, 2, 5.45, 0, 0.0, 9),
  (68,14, 2, 7.95, 0, 0.0, 9),
  (69,13, 1, 9.15, 0, 0.0, 9),
  (70,16, 3, 10.5, 0, 0.0, 9),
  (71,15, 2, 8.55, 0, 0.0, 9),
  (72,9, 3, 6.55, 0, 0.0, 9),
  (73,8, 2, 6.3, 0, 0.0, 9),
  (74,7, 3, 4.5, 0, 0.0, 9),
  (75,11, 3, 6.35, 0, 0.0, 9),
  (76,12, 2, 7.45, 0, 0.0, 9),
  (77,23, 5, 3.5, 0, 0.0, 9);

CREATE VIEW OrdersView AS SELECT ord.id as id, ord.date as order_date, ord.customer_id as customer_id,
  IF(ord.restaurant_state IS NOT NULL, ord.restaurant_state, ord.delivery_state) as state,
  SUM(it.quantity) as  quantity, CAST((SUM(it.price * (it.quantity - it.discount))) AS DECIMAL(5,2)) as cost 
  FROM LittleLemonDB.order ord INNER JOIN LittleLemonDB.order_items it ON ord.id = it.order_id
  WHERE it.canceled = 0 
  GROUP BY ord.id
  HAVING quantity > 1;

CREATE VIEW TopOrders AS SELECT cus.id as customer_id, CONCAT(cus.first_name,' ',cus.last_name) as full_name, 
  ord.id as order_id, ord.date as order_date, 
  CAST((SUM(it.price * (it.quantity - it.discount))) AS DECIMAL(5,2)) as cost, 
  SUM(IF(me.dish_type = 'Starter',it.quantity,0)) as starters_ordered,
  SUM(IF(me.dish_type = 'Course',it.quantity,0)) as courses_ordered,
  SUM(IF(me.dish_type = 'Drink',it.quantity,0)) as drinks_ordered,
  SUM(IF(me.dish_type = 'Dessert',it.quantity,0)) as desserts_ordered
  FROM LittleLemonDB.order ord 
  INNER JOIN LittleLemonDB.order_items it ON ord.id = it.order_id
  INNER JOIN LittleLemonDB.customer cus ON ord.customer_id = cus.id
  INNER JOIN LittleLemonDB.menu me ON it.menu_id = me.id
  WHERE it.canceled = 0 
  GROUP BY ord.id
  HAVING cost > 150
  ORDER BY cost ASC;

CREATE VIEW PopularDishes AS SELECT *
  FROM LittleLemonDB.menu me
  WHERE me.id = ANY
  (SELECT it.menu_id
    FROM LittleLemonDB.order_items it
    GROUP BY it.menu_id
    HAVING COUNT(*) > 2);

PREPARE GetOrderDetail FROM 'SELECT ov.id as order_id, ov.order_date, ov.state, ov.quantity, ov.cost FROM LittleLemonDB.OrdersView ov WHERE ov.customer_id = ? ORDER BY ov.order_date DESC'; 

CREATE PROCEDURE GetMaxQuantity() SELECT ov.quantity FROM LittleLemonDB.OrdersView ov
  ORDER BY ov.quantity DESC
  LIMIT 1;

delimiter //

CREATE PROCEDURE CancelOrder (IN order_id INT)
  BEGIN
    UPDATE LittleLemonDB.order ord SET 
      ord.restaurant_state=IF(ord.restaurant_state IS NOT NULL, 'Canceled', NULL),  
      ord.delivery_state=IF(ord.delivery_state IS NOT NULL, 'Canceled', NULL)
      WHERE ord.id=order_id;
    SELECT CONCAT('Order ', order_id, ' is cancelled') as confirmation;
  END//

CREATE PROCEDURE CheckBooking (IN booking_date DATE, IN bench INT)
  BEGIN
    SELECT CONCAT('The table ', bench, ' is ', IF(COUNT(*) > 0, 'already','not'), ' booked') as confirmation  
    FROM LittleLemonDB.booking bk where bk.date = booking_date and bk.bench_id = bench;
  END//

CREATE PROCEDURE AddValidBooking (IN booking_datetime DATETIME, IN bench INT, IN customer INT)
  BEGIN
    SET @booked := (SELECT COUNT(*) FROM LittleLemonDB.booking bk where bk.date = CAST(booking_datetime as date) and bk.bench_id = bench);
    START TRANSACTION;
      INSERT INTO `LittleLemonDB`.`booking` (`date`, `slot`, `bench_id`, `customer_id`) 
        VALUES (CAST(booking_datetime as date), CAST(booking_datetime as time), bench, customer);  
      IF  @booked = 0 THEN
        COMMIT;
        SELECT CONCAT('You have been nooked the table ', bench, ' - booking successfull') as booking_status;
      ELSE
        ROLLBACK;
        SELECT CONCAT('The table ', bench, ' is already booked - booking cancelled') as booking_status;
      END IF;  
  END//

CREATE PROCEDURE UpdateBooking (IN booking_id INT, IN booking_date DATE)
  BEGIN
    UPDATE LittleLemonDB.booking bk SET bk.date=booking_date WHERE bk.id=booking_id;
    SELECT CONCAT('Booking ', booking_id, ' updated') as confirmation;
  END//

CREATE PROCEDURE CancelBooking  (IN booking_id INT)
  BEGIN
    DELETE FROM LittleLemonDB.booking bk where bk.id = booking_id;
    SELECT CONCAT('Booking ', booking_id, ' cancelled') as confirmation;
  END//

delimiter ;