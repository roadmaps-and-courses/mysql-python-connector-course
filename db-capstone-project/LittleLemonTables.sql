-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema LittleLemonDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema LittleLemonDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `LittleLemonDB` ;
USE `LittleLemonDB` ;

-- -----------------------------------------------------
-- Table `LittleLemonDB`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `contact_number` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`bench`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`bench` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `seatings` INT NOT NULL DEFAULT 4,
  `requires_reservation` TINYINT NOT NULL DEFAULT 0,
  `is_outside` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `slot` TIME NOT NULL,
  `bench_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_2_idx` (`customer_id` ASC) VISIBLE,
  INDEX `fk_booking_3_idx` (`bench_id` ASC) VISIBLE,
  CONSTRAINT `fk_booking_2`
    FOREIGN KEY (`customer_id`)
    REFERENCES `LittleLemonDB`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_3`
    FOREIGN KEY (`bench_id`)
    REFERENCES `LittleLemonDB`.`bench` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `anual_salary` DECIMAL(8,2) NOT NULL,
  `overtime` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`staff` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `role_id` INT NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_staff_1_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_staff_2_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_staff_1`
    FOREIGN KEY (`city_id`)
    REFERENCES `LittleLemonDB`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_staff_2`
    FOREIGN KEY (`role_id`)
    REFERENCES `LittleLemonDB`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` DATE NOT NULL,
  `restaurant_state` ENUM('Received', 'Canceled', 'Attended', 'Paid') NULL,
  `delivery_state` ENUM('Received', 'Canceled', 'Preparing', 'On delivery', 'Delivered') NULL,
  `staff_id` INT NOT NULL,
  `customer_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_1_idx` (`staff_id` ASC) VISIBLE,
  INDEX `fk_order_2_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `LittleLemonDB`.`staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_2`
    FOREIGN KEY (`customer_id`)
    REFERENCES `LittleLemonDB`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`delivery`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`delivery` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stimated_delivery_time` DATETIME NOT NULL,
  `order_id` INT NOT NULL,
  `delivered_time` DATETIME NULL,
  `address` VARCHAR(255) NOT NULL,
  `city_id` INT NOT NULL,
  `delivery_cost` DECIMAL(5,2) NOT NULL DEFAULT 0.0,
  `staff_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_delivery_1_idx` (`order_id` ASC) VISIBLE,
  INDEX `fk_delivery_2_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_delivery_3_idx` (`staff_id` ASC) VISIBLE,
  CONSTRAINT `fk_delivery_1`
    FOREIGN KEY (`order_id`)
    REFERENCES `LittleLemonDB`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_2`
    FOREIGN KEY (`city_id`)
    REFERENCES `LittleLemonDB`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_3`
    FOREIGN KEY (`staff_id`)
    REFERENCES `LittleLemonDB`.`staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`subcategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`subcategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `category_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_subcategory_1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_subcategory_1`
    FOREIGN KEY (`category_id`)
    REFERENCES `LittleLemonDB`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL DEFAULT 0.0,
  `description` TEXT NULL,
  `subcategory_id` INT NOT NULL,
  `is_vegan` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_menu_1_idx` (`subcategory_id` ASC) VISIBLE,
  CONSTRAINT `fk_menu_1`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `LittleLemonDB`.`subcategory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LittleLemonDB`.`order_items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LittleLemonDB`.`order_items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `menu_id` INT NOT NULL,
  `quantity` INT NOT NULL DEFAULT 1,
  `price` DECIMAL(5,2) NOT NULL,
  `canceled` TINYINT NOT NULL DEFAULT 0,
  `discount` DECIMAL(3,2) NOT NULL DEFAULT 0.0,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_items_1_idx` (`menu_id` ASC) VISIBLE,
  INDEX `fk_order_items_2_idx` (`order_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_items_1`
    FOREIGN KEY (`menu_id`)
    REFERENCES `LittleLemonDB`.`menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_items_2`
    FOREIGN KEY (`order_id`)
    REFERENCES `LittleLemonDB`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
