# Mysql/Python Connector 

To work in this repository:

## Run docker database

```bash
# start the mongo instance
sudo docker-compose up -d
# stop the mongo instance
sudo docker-compose down
# erase all instances of docker
sudo docker system prune -a
# erase the volumes
sudo docker volume rm $(sudo docker volume ls -q)
```

## Virtual env management

```bash
# Create a virtual env to run the project
python3 -m venv ./.env
# Enable the virtual env
# Documentation on: https://docs.python.org/es/3/library/venv.html
source ./.env/bin/activate
# Install the requirements
pip install -r requirements.txt
# Run the notebook
python3 -m notebook
# Disable the virtual env
deactivate
```